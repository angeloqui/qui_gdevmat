class Matter {
  
  PVector position;
  PVector colour; //x, y, z for rgb
  int size;
  
  void reset() {
    //Reset matter position at new random
    
    //Limited to 2x the screen size, to make it a little more scattered
    position = new PVector(randomGaussian() * (Window.windowWidth), 
                           randomGaussian() * Window.windowHeight);
    size = func.intRandom(5, 25);
    colour = new PVector(noise(hex.x), noise(hex.y), noise(hex.z));
  }
  
  void render() {    
    PVector direction = PVector.sub(blackHole, position);
    direction.normalize();
    direction.mult(10); //Speed of matters drawn to black hole
    position.add(direction);
    func.mfill(colour.x, colour.y, colour.z);
    circle(position.x, position.y, size);
  } 
}
