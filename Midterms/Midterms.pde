PVector blackHole = new PVector(0, 0);
PVector hex = new PVector(0, 100, 500);
Functions func = new Functions();
Matter[] matters;

void setup() {
  size(1280, 720, P3D); //1080p cant fit on my monitor.. sad
  camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  
  //Initialize
  matters = new Matter[100];
  for (int i = 0; i < matters.length; i++) {
    matters[i] = new Matter();
  }

  background(0);
  noStroke();
  reset();
}

void reset() {    
    background(0);
    
    //Create a blackhole at new random
    //0.0 to 1.0 * window width or height with blackhole size offset
    //divided by 2 for left and right screen
    blackHole.x = randomGaussian() * ((Window.windowWidth / 2) - 50);
    blackHole.y = randomGaussian() * ((Window.windowHeight / 2) - 50);
    
    //Added Perlin for Coverage example
    for (int i = 0; i < matters.length; i++) {
      matters[i].reset();
      hex.add(new PVector(0.02, 0.05, 0.08));
    }
}

void draw() {
  background(0);
  
  if (frameCount % 160 == 0) {
    reset();
  }
    
  for (int i = 0; i < matters.length; i++) {
    matters[i].render();
  }
    
  //Draw Black Hole
  fill(255);
  circle(blackHole.x, blackHole.y, 50);
}
