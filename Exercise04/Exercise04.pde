Mover mover = new Mover();

float acceleration_rate = 0.1;
float deceleration_rate = -0.5;

void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  noStroke();
  
  mover.position.x = Window.left + mover.scale;
  mover.acceleration = new PVector(acceleration_rate, 0);
}

void draw() {
  background(0);
  mover.render();
  
  if (mover.position.x > 0) {
    if (mover.velocity.x > 0) {
      mover.acceleration = new PVector(deceleration_rate, 0);
    } else {
      mover.stop();
    }
  }
}
