void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  background(0);
}

Functions func = new Functions();
PVector position = new PVector(0, 0);

//x, y & size
float[] dt = {0, 50, 100};

//rgb
float rhex = 0;
float ghex = 100;
float bhex = 500;

void draw() {
  for (int i = 0; i < dt.length; i++) {
    dt[i] += 0.01;
  }
  
  position.x = map(noise(dt[0]), 0, 1, Window.left, Window.right);
  position.y = map(noise(dt[1]), 0, 1, Window.bottom, Window.top);
  float size = map(noise(dt[2]), 0, 1, 75, 175);
  
  rhex += 0.02;
  ghex += 0.05;
  bhex += 0.08;

  func.mfill(noise(rhex), noise(ghex), noise(bhex));
  func.render(position.x, position.y, size);
}
