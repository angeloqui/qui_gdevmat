class Functions {
  
  //Draws a circle
  void render(float x, float y, float radius) {
    noStroke();
    circle(x, y, radius);
  }
  
  void mfill(float x, float y, float z) {
    fill(map(x, 0, 1, 0, 255),
         map(y, 0, 1, 0, 255),
         map(z, 0, 1, 0, 255),
         255);
  }
    
  //Clear screen - Black
  void cls(int value) {
    if (frameCount % value == 0) {
      background(0);
    }
  }
  
  //Clear screen with color as parameter
  void cls(int value, PVector colour) {
    if (frameCount % value == 0) {
      background(colour.x, colour.y, colour.z);
    }
  }
}
