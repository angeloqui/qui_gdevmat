Mover[] mover;

float acceleration_rate = 0.1;
float deceleration_rate = -0.5;

void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  noStroke();
  
  mover = new Mover[10];
  
  for (int i = 0; i < mover.length; i++) {
    mover[i] = new Mover();
    mover[i].position.x = Window.left + mover[i].scale;
    //mover[i].acceleration = new PVector(acceleration_rate, 0);
  }
}

PVector wind = new PVector(0.1f, 0);
PVector gravity = new PVector(0, -1);

void draw() {
  background(0);
  
  for (int i = 0; i < mover.length; i++) {
    mover[i].render();
  
    mover[i].applyForce(wind);
    mover[i].applyForce(gravity);
    
    if (mover[i].position.y < Window.bottom) {
      mover[i].velocity.y *= -1;
      mover[i].position.y = Window.bottom;
    }
  
    if (mover[i].position.x > Window.right) {
      mover[i].velocity.x *= -1;
      mover[i].position.x = Window.right;
    }
  }
}
