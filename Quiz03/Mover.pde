class Mover {
  
  public PVector position = new PVector();
  public PVector velocity = new PVector(); 
  public PVector acceleration = new PVector();
  
  public float mass;
  public float scale = 50;
  public float r = 255, g = 255, b = 255, a = 255;
  
  //controllers
  float minSize = 1;
  float maxSize = 10;
  float size_modifier = 0.2;
  
  Mover() {
    mass = random(minSize, maxSize + 1);
    scale *= mass * size_modifier;
    
    //random color
    r = floor(random(255));
    g = floor(random(255));
    b = floor(random(255));
  }
  
  public void render() {
    update();
    fill(r,g,b,a);
    circle(position.x, position.y, scale);  
  }
  
  private void update() {    
    this.velocity.add(this.acceleration);
    this.position.add(this.velocity);
    
    this.acceleration.mult(0);
  }
  
  public void applyForce(PVector force) {
    PVector f = PVector.div(force, mass);
    acceleration.add(f);
  }
}
