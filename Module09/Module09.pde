Mover[] mover;

float margin = 100;
PVector wind = new PVector(0.1f, 0);

void setup() {
  size(1360, 768, P3D);
  camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  noStroke();
  
  mover = new Mover[5];
  
  for (int i = 0; i < mover.length; i++) {
    mover[i] = new Mover();
    
    //Initialize Postions
    mover[i].position.x = Window.left + margin;
    mover[i].position.y = Window.top - margin - ((margin + 40) * i);
  }
}

void draw() {
  background(255);
  
  for (int i = 0; i < mover.length; i++) {
    mover[i].render();

    if (mover[i].position.x < 0) {    
      mover[i].applyForce(wind);
    } else {
      breaker(mover[i]);
    }
  }
  
}

public void breaker(Mover mover) {
  //Cushon; to prevent infinitesimal velocity from stacking 
  if (mover.velocity.x < 0) {
    mover.applyFriction(-mover.velocity.x);
    return;
  }
  
  mover.applyFriction(0.15f);
}
