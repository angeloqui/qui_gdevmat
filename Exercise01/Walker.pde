class Walker {
  float xPosition;
  float yPosition;
  
  void render() {
    fill(intRandom(255), intRandom(255), intRandom(255), intRandom(255));
    circle(xPosition, yPosition, 30);
  }
  
  void randomWalk(int stepSize) {
    int decision = intRandom(8);
    
    if (decision == 0) {
      yPosition += stepSize;
    } else if (decision == 1) {
      yPosition -= stepSize;
    } else if (decision == 2) {
      xPosition -= stepSize;
    } else if (decision == 3) {
      xPosition += stepSize;
    } else if (decision == 4) {
      xPosition -= stepSize;
      yPosition += stepSize;
    } else if (decision == 5) {
      xPosition += stepSize;
      yPosition += stepSize;
    } else if (decision == 6) {
      xPosition -= stepSize;
      yPosition -= stepSize;
    } else if (decision == 7) {
      xPosition += stepSize;
      yPosition -= stepSize;
    }
  }
  
  int intRandom(int value) {
    return floor(random(value));
  }
  
}
