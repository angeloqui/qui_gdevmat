/*Functions template
Works the same as quiz 01, but will be imporved based on the next activity
*/

void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  background(0);
}

Vector2 mousePos() {
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x, y);
}

Functions func = new Functions();
Vector2 position = new Vector2(0, 0);

void draw() {
  func.move(position);
  func.render(position.x, position.y, func.normalDistribution(100));
  func.cls(1000);
}
