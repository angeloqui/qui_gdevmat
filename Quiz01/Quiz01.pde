void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  background(0);
}

int intRandom(int value) {
    return floor(random(value));
}

void render(float x, float y, float radius) {
    fill(intRandom(255), intRandom(255), intRandom(255), intRandom(255));
    noStroke();
    circle(x, y, radius);
}

void draw() {
  float gauss = randomGaussian();
  float standardDeviation = width;
  float mean = 0;
  
  float xPos = (standardDeviation * gauss) + mean;
  float yPos = random(-height / 2, height / 2); 
  
  float radius = (100 * randomGaussian()) + 0;
  
  render(xPos, yPos, radius);
  
  //Bonus
  println(radius);
  if (frameCount % 1000 == 0) {
    background(0);
  }
}
