public class Vector2 {
  public float x;
  public float y;
  
  Vector2() {
    x = 0;
    y = 0;
  }
  
  Vector2(float x, float y) {
    this.x = x;
    this.y = y;
  }
  
  public Vector2 add(Vector2 u) {
    this.x += u.x;
    this.y += u.y;
    return this;
  }
  
  public Vector2 subtract(Vector2 u) {
    this.x -= u.x;
    this.y -= u.y;
    return this;
  }
  
  public Vector2 multiply(float scalar) {
    this.x *= scalar;
    this.y *= scalar;
    return this;
  }
  
  public Vector2 divide(float scalar) {
    this.x /= scalar;
    this.y /= scalar;
    return this;
  }
  
  public float magnitude() {
    return sqrt((x * x) + (y * y));
  }
  
  public Vector2 normalize() {
    float length = this.magnitude();
    if (length > 0) {
      this.divide(length);
    }
    return this;
  }
}
