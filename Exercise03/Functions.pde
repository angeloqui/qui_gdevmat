class Functions {
  
  //Draws a circle
  void render(float x, float y, float radius) {
    fill(intRandom(255), intRandom(255), intRandom(255), intRandom(255));
    noStroke();
    circle(x, y, radius);
  }
  
  //Range between 0 and value
    int intRandom(int value) {
    return floor(random(value));
}
  
  //Range between min and max
  int intRandom(int min, int max) {
    return floor(random(min, max + 1));
  }
  
  //Normal distribution of standardDeviation(size) at center
  float normalDistribution(float standardDeviation) {
    return (standardDeviation * randomGaussian()) + 0;
  }
  
  //Normal distribution of standardDeviation(size) at mean
  float normalDistribution(float standardDeviation, float mean) {
    return (standardDeviation * randomGaussian()) + mean;
  }
  
  //Clear screen - Black
  void cls(int value) {
    if (frameCount % value == 0) {
      background(0);
    }
  }
  
  //Clear screen with color as parameter
  void cls(int value, PVector colour) {
    if (frameCount % value == 0) {
      background(colour.x, colour.y, colour.z);
    }
  }
}
