void setup() {
  size(1920, 1080, P3D);
  camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  background(0);
}

Functions func = new Functions();
PVector colour = new PVector(0, 100, 500);
float rot = 0;
float dt = 0;

Vector2 size = new Vector2(Window.windowWidth / 2, Window.windowHeight / 2);

void draw() {
  background(0);
  
  //Color
  stroke(map(noise(colour.x), 0, 1, 0, 255), 
         map(noise(colour.y), 0, 1, 0, 255),
         map(noise(colour.z), 0, 1, 0, 255)
         );
  
  //Rotation
  rot = map(noise(dt), 0, 1, 0, 2 * PI);
  rotate(rot);
  
  //Lightsaber
  strokeWeight(15);
  line(0, 0, size.x, size.y);
  line(0, 0, -size.x, -size.y);
  
  //Inside white light
  stroke(255, 255, 255);
  strokeWeight(4);
  line(0, 0, size.x, size.y);
  line(0, 0, -size.x, -size.y);
  
  //Handle
  stroke(100, 100, 100);
  strokeWeight(15);
  line(0, 0, size.x / 4, size.y / 4);
  line(0, 0, -size.x / 4, -size.y / 4);
  
  //Increment
  colour.add(0.01, 0.01, 0.01);
  dt += 0.01;
}
