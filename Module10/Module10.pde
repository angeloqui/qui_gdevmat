Mover[] mover;
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);
PVector wind = new PVector(0.1f, 0);

int intRandom(int min, int max) {
    return floor(random(min, max + 1));
}

int intRandom(int num) {
    return floor(random(num));
}

void setup() {
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  mover = new Mover[10];
  
  for (int i = 0; i < mover.length; i++) {
    mover[i] = new Mover();
    
    //Initialize Postions
    mover[i].position.x = Window.left + 50 + (i * 100);
    mover[i].setColor(intRandom(255), intRandom(255), intRandom(255), 255);
    mover[i].mass = intRandom(1, 10);
  }
}

void draw() {
  background(255);
  
  ocean.render();
  noStroke();
  
  for (int i = 0; i < mover.length; i++) {
    mover[i].applyGravity();
    mover[i].applyFriction();
    
    mover[i].render();
    mover[i].update();
    
    if (mover[i].position.x > Window.right) {
      mover[i].velocity.x *= -1;
      mover[i].position.x = Window.right;
    }
    
    if (mover[i].position.y < Window.bottom) {
      mover[i].velocity.y *= -1; 
      mover[i].position.y = Window.bottom;
    }
    
    if (ocean.isCollidingWith(mover[i])) {
      mover[i].applyForce(ocean.calculateDragForce(mover[i])); 
    } else {
      mover[i].applyForce(wind);
    }
  }
}
